#![warn(clippy::pedantic)]
#![deny(missing_docs)]

//! Network and Network Flow Optimization Tools
//!
//! # Introduction
//!
//! The following example is based on the following graph
//! ![An example graph](https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)
//! sourced from [Wikipedia][https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm].
//!
//! Here, the goal is to find the [shortest path tree](https://en.wikipedia.org/wiki/Shortest-path_tree) rooted at node 0.
//! In the example, it is verified the distance from node 0 to node 4 is 20.
//!
//! ```
//! use spokes::{Network, algorithms::{dijkstra_shortest_path, Distance}, ArcStorage};
//!
//! let mut network: Network<usize, (), u16> = Network::new();
//! network.add_nodes((0..6).map(|i| (i, ())));
//! network.add_arcs([
//!     (0, 1, 7), (1, 0, 7),
//!     (0, 5, 14), (5, 0, 14),
//!     (0, 2, 9), (2, 0, 9),
//!     (1, 2, 10), (2, 1, 10),
//!     (1, 3, 15), (3, 1, 15),
//!     (2, 5, 2), (5, 2, 2),
//!     (2, 3, 11), (3, 2, 11),
//!     (3, 4, 6), (4, 3, 6),
//!     (4, 5, 9), (5, 4, 9),
//! ]);
//!
//! let shortest_path_tree = dijkstra_shortest_path(&network, 0);
//!
//! assert_eq!(shortest_path_tree.node(&4), Some(&Distance::Finite(20)));
//!
//! let mut expected_network: Network<usize, Distance<u16>, ()> = Network::new();
//!
//! expected_network.add_nodes([
//!     (0, Distance::Finite(0)),
//!     (1, Distance::Finite(7)),
//!     (2, Distance::Finite(9)),
//!     (3, Distance::Finite(20)),
//!     (4, Distance::Finite(20)),
//!     (5, Distance::Finite(11)),
//! ]);
//!
//! expected_network.add_arcs([
//!     (1, 0),
//!     (2, 0),
//!     (3, 2),
//!     (5, 2),
//!     (4, 5),
//! ]);
//!
//! assert_eq!(shortest_path_tree, expected_network);
//! ```

pub mod algorithms;
pub mod arc_storage;
pub mod datasets;
pub mod search;

mod arc_info;
use std::{
    collections::{hash_map, HashMap},
    hash::Hash,
    marker::PhantomData,
    ops::Index,
};

pub use arc_storage::{ArcStorage, HashMapStorage};
use search::{BfsIter, DfsIter, Direction};

pub use self::arc_info::ArcInfo;

#[derive(Clone, Debug)]
#[must_use]
/// A representation of a network with node and arc attributes
pub struct Network<I, NA, AA, ArcStore = HashMapStorage<I, AA>>
where
    ArcStore: ArcStorage<I, AA>,
{
    _phantom_aa: PhantomData<AA>,
    nodes: HashMap<I, NA>,
    arc_store: ArcStore,
}

impl<I, NA, AA> Network<I, NA, AA, HashMapStorage<I, AA>>
where
    I: Hash + Eq + Copy,
{
    /// Create a new __Empty__ network.
    pub fn new() -> Self {
        Self::default()
    }

    /// Create a new __Empty__ network with given capacities.
    pub fn with_capacity(n_nodes: usize, m_arcs: usize) -> Self {
        Self {
            _phantom_aa: PhantomData,
            nodes: HashMap::with_capacity(n_nodes),
            arc_store: HashMapStorage::with_capacity(n_nodes, m_arcs),
        }
    }
}

impl<I, NA, AA, ArcStore> Network<I, NA, AA, ArcStore>
where
    ArcStore: ArcStorage<I, AA>,
    I: Hash + Eq + Copy,
{
    /// Get a reference to the network's nodes.
    #[must_use]
    pub fn nodes(&self) -> &HashMap<I, NA> {
        &self.nodes
    }

    /// Get a reference to the network's arc store.
    #[must_use]
    pub fn arc_store(&self) -> &ArcStore {
        &self.arc_store
    }

    /// Return the number of nodes in the network.
    pub fn n_nodes(&self) -> usize {
        self.nodes.len()
    }

    /// Add a node to the network.
    ///
    /// This will return the node attributes for a replaced node.
    pub fn add_node(&mut self, id: I, attributes: NA) -> Option<NA> {
        self.nodes.insert(id, attributes)
    }

    /// Convert the network into it's constituent parts.
    pub fn into_parts(self) -> (HashMap<I, NA>, ArcStore) {
        (self.nodes, self.arc_store)
    }

    /// Create a [`Network`] from node and arc storage.
    pub fn from_parts(nodes: HashMap<I, NA>, arc_store: ArcStore) -> Self {
        Self {
            _phantom_aa: PhantomData,
            nodes,
            arc_store,
        }
    }

    /// Add nodes from an iterator.
    pub fn add_nodes<T: IntoIterator<Item = (I, NA)>>(&mut self, from: T) {
        from.into_iter().for_each(|(id, attr)| {
            self.add_node(id, attr);
        });
    }

    /// Remove a node.
    ///
    /// If the node is a part of an arc, the arc will be removed as well.
    pub fn remove_node(&mut self, id: &I) -> Option<NA> {
        let arcs_to_remove: Vec<(I, I)> = self
            .arc_store
            .forward_arcs(id)
            .map(|a| (a.tail, a.head))
            .chain(self.arc_store.reverse_arcs(id).map(|a| (a.tail, a.head)))
            .collect();

        self.arc_store.remove_arcs(arcs_to_remove);

        self.nodes.remove(id)
    }

    /// Test if a given node id is within the network
    pub fn contains_node(&self, id: &I) -> bool {
        self.nodes.contains_key(id)
    }

    /// Access a particular node's attributes.
    pub fn node(&self, id: &I) -> Option<&NA> {
        self.nodes.get(id)
    }

    /// Gets the given node's corresponding entry in the node map for in-place manipulation.
    pub fn node_entry(&mut self, id: I) -> hash_map::Entry<'_, I, NA> {
        self.nodes.entry(id)
    }

    /// Access a particular node's attributes, mutibly.
    pub fn node_mut(&mut self, id: &I) -> Option<&mut NA> {
        self.nodes.get_mut(id)
    }

    /// Return an iterator through the nodes and their attributes
    pub fn iter_nodes(&self) -> impl Iterator<Item = (&I, &NA)> {
        self.nodes.iter()
    }

    /// Return an iterator that performs a breadth first search of the network starting from
    /// `start_id`.
    pub fn bfs<'a>(
        &'a self,
        start_id: &'a I,
        direction: Direction,
    ) -> BfsIter<'a, I, AA, ArcStore> {
        BfsIter::new(&self.arc_store, start_id, direction)
    }

    /// Return an iterator that performs a depth first search of the network starting from
    /// `start_id`.
    pub fn dfs<'a>(
        &'a self,
        start_id: &'a I,
        direction: Direction,
    ) -> DfsIter<'a, I, AA, ArcStore> {
        DfsIter::new(&self.arc_store, start_id, direction)
    }

    /// Consume this network and transform it's nodes with a Fn.
    pub fn map_nodes<F, NAP>(self, f: F) -> Network<I, NAP, AA, ArcStore>
    where
        F: Fn((I, NA)) -> (I, NAP),
    {
        Network {
            _phantom_aa: PhantomData,
            nodes: self.nodes.into_iter().map(f).collect(),
            arc_store: self.arc_store,
        }
    }

    /// Consume this network and transform it's arcs with a Fn.
    pub fn map_arcs<F, AAP, ASP>(self, f: F) -> Network<I, NA, AAP, ASP>
    where
        F: Fn(ArcInfo<I, AA>) -> ArcInfo<I, AAP>,
        ASP: ArcStorage<I, AAP> + FromIterator<ArcInfo<I, AAP>>,
        ArcStore: IntoIterator<Item = ArcInfo<I, AA>>,
    {
        Network {
            _phantom_aa: PhantomData,
            nodes: self.nodes,
            arc_store: self.arc_store.into_iter().map(f).collect(),
        }
    }

    /// Consume a network and transform it's nodes and arcs with Fns.
    pub fn map_nodes_and_arcs<FN, FA, NAP, AAP, ASP>(
        self,
        f_nodes: FN,
        f_arcs: FA,
    ) -> Network<I, NAP, AAP, ASP>
    where
        FN: Fn((I, NA)) -> (I, NAP),
        FA: Fn(ArcInfo<I, AA>) -> ArcInfo<I, AAP>,
        ASP: ArcStorage<I, AAP> + FromIterator<ArcInfo<I, AAP>>,
        ArcStore: IntoIterator<Item = ArcInfo<I, AA>>,
    {
        Network {
            _phantom_aa: PhantomData,
            nodes: self.nodes.into_iter().map(f_nodes).collect(),
            arc_store: self.arc_store.into_iter().map(f_arcs).collect(),
        }
    }
}

impl<I, NA, AA, ArcStore> Default for Network<I, NA, AA, ArcStore>
where
    ArcStore: ArcStorage<I, AA>,
{
    fn default() -> Self {
        Self {
            _phantom_aa: PhantomData,
            nodes: HashMap::new(),
            arc_store: ArcStore::default(),
        }
    }
}

impl<I, NA, AA, ArcStore> ArcStorage<I, AA> for Network<I, NA, AA, ArcStore>
where
    ArcStore: ArcStorage<I, AA>,
    I: Hash + Eq + Copy,
{
    fn with_capacity(n_nodes: usize, m_arcs: usize) -> Self {
        Self {
            _phantom_aa: PhantomData,
            nodes: HashMap::with_capacity(n_nodes),
            arc_store: ArcStorage::with_capacity(n_nodes, m_arcs),
        }
    }

    fn m_arcs(&self) -> usize {
        self.arc_store.m_arcs()
    }

    fn add_arc<T: Into<ArcInfo<I, AA>>>(&mut self, arc: T) {
        let arc = arc.into();
        assert!(
            self.contains_node(&arc.tail) && self.contains_node(&arc.head),
            "Only arcs between exisiting nodes may be added."
        );

        self.arc_store.add_arc(arc);
    }

    fn remove_arc(&mut self, tail: I, head: I) -> Option<AA> {
        self.arc_store.remove_arc(tail, head)
    }

    fn arc_iter<'a>(&'a self) -> Box<dyn Iterator<Item = &'a ArcInfo<I, AA>> + 'a> {
        self.arc_store.arc_iter()
    }

    fn arc_iter_mut<'a>(&'a mut self) -> Box<dyn Iterator<Item = &'a mut ArcInfo<I, AA>> + 'a> {
        self.arc_store.arc_iter_mut()
    }

    fn forward_arcs<'a>(
        &'a self,
        node: &'a I,
    ) -> Box<dyn Iterator<Item = &'a ArcInfo<I, AA>> + 'a> {
        self.arc_store.forward_arcs(node)
    }

    fn reverse_arcs<'a>(
        &'a self,
        node: &'a I,
    ) -> Box<dyn Iterator<Item = &'a ArcInfo<I, AA>> + 'a> {
        self.arc_store.reverse_arcs(node)
    }

    fn arc(&self, tail: I, head: I) -> Option<&AA> {
        self.arc_store.arc(tail, head)
    }

    fn arc_mut(&mut self, tail: I, head: I) -> Option<&mut AA> {
        self.arc_store.arc_mut(tail, head)
    }

    fn contains_arc(&self, tail: I, head: I) -> bool {
        self.arc_store.contains_arc(tail, head)
    }
}

impl<I, NA, AA, AS> PartialEq for Network<I, NA, AA, AS>
where
    AS: ArcStorage<I, AA> + PartialEq,
    I: Hash + Eq + Copy,
    NA: PartialEq,
    AA: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.nodes == other.nodes && self.arc_store == other.arc_store
    }
}

/// Temporary structure used to access a network's nodes
pub struct NodeAccessor<'a, I, NA, AA, ArcStore>
where
    ArcStore: ArcStorage<I, AA>,
{
    network: &'a Network<I, NA, AA, ArcStore>,
}

impl<'a, I, NA, AA, ArcStore> Index<&I> for NodeAccessor<'a, I, NA, AA, ArcStore>
where
    ArcStore: ArcStorage<I, AA>,
    I: Hash + Eq,
{
    type Output = NA;

    fn index(&self, index: &I) -> &Self::Output {
        self.network.nodes.get(index).unwrap()
    }
}
