/// Information about an Arc
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ArcInfo<I, A> {
    /// Tail Node
    pub tail: I,
    /// Head Node
    pub head: I,
    /// Attributes
    pub attributes: A,
}

impl<I, A> ArcInfo<I, A> {
    /// Create a new `ArcInfo` with the given tail -> head link with the given attributes.
    pub fn new(tail: I, head: I, attributes: A) -> Self {
        Self {
            tail,
            head,
            attributes,
        }
    }

    /// Get the arc's head.
    pub fn head(&self) -> &I {
        &self.head
    }

    /// Get the arc's tail.
    pub fn tail(&self) -> &I {
        &self.tail
    }

    /// Get a reference to the arc info's attributes.
    pub fn attributes(&self) -> &A {
        &self.attributes
    }
}

impl<I, A> From<(I, I, A)> for ArcInfo<I, A> {
    fn from(val: (I, I, A)) -> Self {
        ArcInfo::new(val.0, val.1, val.2)
    }
}

impl<I> From<(I, I)> for ArcInfo<I, ()> {
    fn from(val: (I, I)) -> Self {
        ArcInfo::new(val.0, val.1, ())
    }
}
